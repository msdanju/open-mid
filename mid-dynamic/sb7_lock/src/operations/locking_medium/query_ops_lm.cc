#include "query_ops_lm.h"
#include<iostream>
#include "../../helpers.h"
#include "../../containers.h"
#include "../../parameters.h"
#include "../../struct/assembly.h"
#include "lock_srv_lm.h"
#include "../../thread/thread.h"
#include<pthread.h>
#include <stdio.h>
#include "../../interval.h"
#include <vector>
////////////
// Query1 //
////////////
 #define QUERY1_ITER 10 
// Anju - try to use numNodesLocked via command line arg.

extern IntervalCheck ICheck;
extern int numBuckets;
extern int numIterations;
extern int numNodesLocked;

/*extern int falseSubsumptionCount;
extern int *fsCountArray;
extern int *correctedFScountArray; */

int sb7::LMQuery1::run(int tid) const {
	//ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	//ReadLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());
//cout<<"/n name is"<<name;
	return innerRun(tid);
}

int sb7::LMQuery1::innerRun(int tid) const {
	int count = 0;

int threadID = tid;
//****************************************************************************************************************************************
	int min=0,max=0;

	//int compId = get_random()->nextInt(300)+1; // Anju commented this to change random generation part
	//***************************************************************************************
	std::vector<int> apartId;
	apartId.reserve(numNodesLocked);
	apartId.resize(numNodesLocked);
	for(int a=0; a < numNodesLocked; a++)
	{
			apartId[a] = 0;
	}
//	int apartId[QUERY1_ITER] = {0};
	//***************************************************************************************
	// Anju added -----start -----
			int bucketNum = get_random()->nextInt(numBuckets);
			int bucketSize = parameters.getInitialTotalCompParts() * parameters.getNumAtomicPerComp() / numBuckets; 
	//		printf("numBuckets = %d, bucketSize = %d\n\n", numBuckets, bucketSize);
//			int bucketSize = parameters.getMaxAtomicParts() / numBuckets;
	// Anju added -----end -----
	for(int i = 0; i < numNodesLocked; i++) {
			// Anju ---- start ----
				int randNumInFirstBucket = get_random()->nextInt(bucketSize);
			apartId[i] = bucketNum * bucketSize + randNumInFirstBucket; // (bucketNum -1 ) * bucketSize will give the starting id in the bucket of the current thread. 
			// Anju ---- end ----
		}

	for(int i = 0; i < numNodesLocked; i++) {
		Map<int, AtomicPart *> *apartInd = dataHolder->getAtomicPartIdIndex();
		Map<int, AtomicPart *>::Query query;
		query.key = apartId[i];
		apartInd->get(query);

		if(query.found) {
			
			if((((query.val)-> getPartOf()) -> getUsedIn())->size() != 0)// if not a disconnected composite part
			{
				if(min == 0 && max == 0)
				{
				min = (query.val)-> m_pre_number;
				max = (query.val)-> m_post_number;
				}
				else {
					if((query.val)-> m_pre_number < min )
					min = (query.val)-> m_pre_number;
					if((query.val)-> m_post_number > max )
					max = (query.val)-> m_post_number;
				}
			}


			//performOperationOnAtomicPart(query.val);
			//count++;
		}
	}


	if( min == 0 || max == 0)
	{
	return 0;
	}
	int rlm_min, rlm_max;
	pthread_rwlock_t *lock = getDominatorLock(&min,&max, &rlm_min, &rlm_max);
	//IMPORTANT NOTE: After the call to getDominatorLock function, the values of min and max can change. They will become the low and high values of the interval of the corresponding dominator.

	//interval *inv = new interval(min,max);
	// Anju testing...
	/*if(min == 1 && max == 495)
	{
	printf("Thread %d: bucketSize = %d, bucketnum = %d IDs generated in first bucket are:\n %d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n********\n", threadID, bucketSize, bucketNum, firstBucketIDs[0], firstBucketIDs[1], firstBucketIDs[2], firstBucketIDs[3], firstBucketIDs[4], firstBucketIDs[5], firstBucketIDs[6], firstBucketIDs[7], firstBucketIDs[8], firstBucketIDs[9]  );
	printf("Bucket range for thread %d is [%d, %d] \n atomic IDs generated are: \n %d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n-----\n", threadID, bucketNum * bucketSize , (bucketNum + 1) * bucketSize - 1, apartId[0], apartId[1], apartId[2], apartId[3], apartId[4], apartId[5], apartId[6], apartId[7], apartId[8], apartId[9] );
//cout<<"\n Randomness done \n";

	} */
	
if(string(name) == "Q1")	
{
//	xy:while(ICheck.IsWriteOverlap(inv));
	interval *inv = new interval(min,max, rlm_min, rlm_max, 0);		
xy:	if(!ICheck.IsOverlap(inv, 0, threadID, dataHolder))
	{
	
		pthread_rwlock_rdlock(lock);
	
	//ICheck.Insert(inv, threadID);
	
		
	
	
	for(int i = 0; i < numNodesLocked; i++) {
		Map<int, AtomicPart *> *apartInd = dataHolder->getAtomicPartIdIndex();
		Map<int, AtomicPart *>::Query query;
		query.key = apartId[i];
		apartInd->get(query);

		if(query.found) {
		performOperationOnAtomicPart(query.val);
			count++;
		}
	}


	//cout<<"Please HoJA";
	//printf("\n Q1 Inserted %d %d by %d",min,max,threadID);
		
	
	ICheck.Delete(threadID);
	//printf("\n Q1 Deleted %d %d by %d",min,max,threadID);
	pthread_rwlock_unlock(lock);
//printf("\n Q1 Unlocked %d %d by %d",min,max,threadID);
	}
	else 
	{
			//printf("\n Q1 Overlap %d %d by %d",min,max,threadID);
			//sleep(get_random()->nextInt(10)); // Anju uncommented this line for testing			
			sleep(get_random()->nextInt(10)); // Anju uncommented this line for testing			
			goto xy;
	}
	
}

else if(string(name) == "OP9"|| string(name) == "OP15")
{

interval *inv = new interval(min,max, rlm_min, rlm_max, 1);
//xyz:while(ICheck.IsReadOverlap(inv));
	
xyz:	if(!ICheck.IsOverlap(inv, 1, threadID, dataHolder))
	{

	pthread_rwlock_wrlock(lock);	
	//ICheck.Insert(inv, threadID);


	
	
	for(int i = 0; i < numNodesLocked; i++) {
		Map<int, AtomicPart *> *apartInd = dataHolder->getAtomicPartIdIndex();
		Map<int, AtomicPart *>::Query query;
		query.key = apartId[i];
		apartInd->get(query);

		if(query.found) {
		performOperationOnAtomicPart(query.val);
			count++;
		}
	}


	//cout<<"Please HoJA";
	//printf("\nOP9 Inserted %d %d by %d",min,max,threadID);
	
	ICheck.Delete(threadID);
	//printf("\n OP9 Deleted %d %d by %d",min,max,threadID);
	pthread_rwlock_unlock(lock);	
     //  printf("\n OP9 Unlocked %d %d by %d",min,max,threadID);
	}
	else 
	{
			sleep(get_random()->nextInt(10)); // Anju uncommented this line for testing
			//printf("\n OP9 Overlap %d %d by %d",min,max,threadID);

			goto xyz;
	}
	


}



//***********************************************************************************************************************************


//	printf("Thread %d is done.....\n", threadID);
	return count;
}

void sb7::LMQuery1::performOperationOnAtomicPart(AtomicPart *apart) const {
//Anju adding for trial:
int x;
for(int i = 0; i < numIterations; i++)
{
	x += i;
}

//************************************************************************************************
//if(((apart -> getPartOf()) -> getUsedIn())->size() != 0)
//cout<<"\n pre post "<< apart -> m_pre_number <<" -- "<< apart -> m_post_number <<"\n";
//************************************************************************************************
apart->nullOperation();
}

////////////
// Query2 //
////////////

sb7::LMQuery2::LMQuery2(DataHolder *dh, optype t, const char *n, int percent)
		: Operation(t, n, dh) {
	maxAtomicDate = parameters.getMaxAtomicDate();
	minAtomicDate = parameters.getMaxAtomicDate() -
		percent * (parameters.getMaxAtomicDate() -
					parameters.getMinAtomicDate()) / 100;
}

int sb7::LMQuery2::run(int tid) const {
	//ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	//ReadLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());

	return innerRun(tid);
}

int sb7::LMQuery2::innerRun(int tid) const {
	int count = 0;
	int threadID = tid;

	int min=0,max=0;


	Map<int, Set<AtomicPart *> *> *setInd =
		dataHolder->getAtomicPartBuildDateIndex();
	MapIterator<int, Set<AtomicPart *> *> iter =
		setInd->getRange(minAtomicDate, maxAtomicDate);

	while(iter.has_next()) {
		Set<AtomicPart *> *apartSet = iter.next();
		SetIterator<AtomicPart *> apartIter = apartSet->getIter();

		while(apartIter.has_next()) {
			AtomicPart *apart = apartIter.next();

//***************************************************************************************************************************************

			if((((apart)-> getPartOf()) -> getUsedIn())->size() != 0)// if not a disconnected composite part
			{
				if(min == 0 && max == 0)
				{
				min = (apart)-> m_pre_number;
				max = (apart)-> m_post_number;
				}
				else {
					if((apart)-> m_pre_number < min )
					min = (apart)-> m_pre_number;
					if((apart)-> m_post_number > max )
					max = (apart)-> m_post_number;
				}
			}





//**************************************************************************************************************************************

			//performOperationOnAtomicPart(apart);
			//count++;
		}
	}

	if(min == 0 || max == 0)
	return 0;
	int rlm_min, rlm_max;
	pthread_rwlock_t *lock = getDominatorLock(&min,&max, &rlm_min, &rlm_max);
	//interval *inv = new interval(min,max);


MapIterator<int, Set<AtomicPart *> *> iter1 =
		setInd->getRange(minAtomicDate, maxAtomicDate);
	
if(string(name) == "Q2")	
{
	//xy:while(ICheck.IsWriteOverlap(inv));
	interval *inv = new interval(min,max, rlm_min, rlm_max, 0);
xy:	if(!ICheck.IsOverlap(inv, 0, threadID, dataHolder))
	{

	
	//ICheck.Insert(inv, threadID);

	pthread_rwlock_rdlock(lock);	
	//printf("\n Q2 Inserted %d %d by %d",min,max,threadID);
	while(iter1.has_next()) {
		Set<AtomicPart *> *apartSet = iter1.next();
		SetIterator<AtomicPart *> apartIter = apartSet->getIter();

		while(apartIter.has_next()) {
			AtomicPart *apart = apartIter.next();
			performOperationOnAtomicPart(apart);
			count++;
		}
	}
	


	//cout<<"Please HoJA";
	
	pthread_rwlock_unlock(lock);	
	
	ICheck.Delete(threadID);
	//printf("\n Q2 Deleted %d %d by %d",min,max,threadID);
	}
	else goto xy;
	
}

else if(string(name) == "OP10")
{

interval *inv = new interval(min,max, rlm_min, rlm_max, 1);

 xyz:	if(!ICheck.IsOverlap(inv,1,threadID, dataHolder))
	{

	
	//ICheck.Insert(inv, threadID);

	pthread_rwlock_wrlock(lock);	
	//printf("\n OP10 Inserted %d %d by %d",min,max,threadID);
	
	while(iter1.has_next()) {
		Set<AtomicPart *> *apartSet = iter1.next();
		SetIterator<AtomicPart *> apartIter = apartSet->getIter();

		while(apartIter.has_next()) {
			AtomicPart *apart = apartIter.next();
			performOperationOnAtomicPart(apart);
			count++;
		}
	}
	

	//cout<<"Please HoJA";
	
	pthread_rwlock_unlock(lock);	

	ICheck.Delete(threadID);
	//printf("\n OP10 Deleted %d %d by %d",min,max,threadID);
	}
	else goto xyz;
	


}







	return count;
}

void sb7::LMQuery2::performOperationOnAtomicPart(AtomicPart *apart) const {
	apart->nullOperation();
}

////////////
// Query4 //
////////////

#define QUERY4_ITER 100

int sb7::LMQuery4::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle bassmLockHandle(lm_lock_srv.getBaseAssemblyLock());

	int ret = 0;

	for(int i = 0;i < QUERY4_ITER;i++) {
		// construct name of documentation for composite part
		int partId = get_random()->nextInt(parameters.getMaxCompParts()) + 1;
		// TODO move all these constants to separate header file
		ITOA(itoa_buf, partId);
		string title = "Composite Part #" + (string)itoa_buf;

		// search for document with that name
		Map<string, Document *> *docInd = dataHolder->getDocumentTitleIndex();
		Map<string, Document *>::Query query;
		query.key = title;
		docInd->get(query);

		if(query.found) {
			Document *doc = query.val;
			CompositePart *cpart = doc->getCompositePart();
			Bag<BaseAssembly *> *usedIn = cpart->getUsedIn();
			BagIterator<BaseAssembly *> iter = usedIn->getIter();

			while(iter.has_next()) {
				BaseAssembly *bassm = iter.next();
				bassm->nullOperation();
				ret++;
			}
		}
	}

	return ret;
}

////////////
// Query5 //
////////////

int sb7::LMQuery5::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle bassmLockHandle(lm_lock_srv.getBaseAssemblyLock());
	ReadLockHandle cpartLockHandle(lm_lock_srv.getCompositePartLock());

	int ret = 0;

	Map<int, BaseAssembly *> *bassmInd = dataHolder->getBaseAssemblyIdIndex();
	MapIterator<int, BaseAssembly *> iter = bassmInd->getAll();

	while(iter.has_next()) {
		ret += checkBaseAssembly(iter.next());
	} 

	return ret;
}

int sb7::LMQuery5::checkBaseAssembly(BaseAssembly *bassm) const {
	int assmBuildDate = bassm->getBuildDate();
	Bag<CompositePart *> *cparts = bassm->getComponents();
	BagIterator<CompositePart *> iter = cparts->getIter();

	while(iter.has_next()) {
		CompositePart *cpart = iter.next();

		if(cpart->getBuildDate() > assmBuildDate) {
			bassm->nullOperation();
			return 1;
		}
	}

	return 0;
}

////////////
// Query6 //
////////////

int sb7::LMQuery6::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle assmLockHandle(lm_lock_srv.getAssemblyLockArray());
	ReadLockHandle cpartLockHandle(lm_lock_srv.getCompositePartLock());
	
	return checkComplexAssembly(dataHolder->getModule()->getDesignRoot());
}

int sb7::LMQuery6::checkAssembly(Assembly *assembly) const {
	if(assembly->getType() == assembly_type_complex) {
		return checkComplexAssembly((ComplexAssembly *)assembly);
	} else {
		return checkBaseAssembly((BaseAssembly *)assembly);
	}
}

int sb7::LMQuery6::checkComplexAssembly(ComplexAssembly *assembly) const {
	int ret = 0;

	Set<Assembly *> *subAssmSet = assembly->getSubAssemblies();
	SetIterator<Assembly *> iter = subAssmSet->getIter();

	while(iter.has_next()) {
		ret += checkAssembly(iter.next());
	}

	if(ret) {
		assembly->nullOperation();
		ret++;
	}

	return ret;
}

////////////
// Query7 //
////////////

int sb7::LMQuery7::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());

	int ret = 0;

	Map<int, AtomicPart *> *apartInd = dataHolder->getAtomicPartIdIndex();
	MapIterator<int, AtomicPart *> iter = apartInd->getAll();

	while(iter.has_next()) {
		AtomicPart *apart = iter.next();
		apart->nullOperation();
		ret++;
	}

	return ret;
}
