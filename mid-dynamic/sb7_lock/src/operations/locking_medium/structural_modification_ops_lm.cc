#include "structural_modification_ops_lm.h"

#include "../../parameters.h"
#include "../../sb7_exception.h"
#include "lock_srv_lm.h"
#include "../../thread/thread.h"
#include "../../interval.h"
#include<pthread.h>

extern IntervalCheck ICheck;
/////////////////////////////
// StructuralModification1 //
/////////////////////////////

int sb7::LMStructuralModification1::run(int tid) const {
	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());

	dataHolder->createCompositePart();
	return 0;
}

/////////////////////////////
// StructuralModification2 //
/////////////////////////////

int sb7::LMStructuralModification2::run(int tid) const {
	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());

	// generate random composite part id and try to look it up
	int cpartId = get_random()->nextInt(parameters.getMaxCompParts()) + 1;
	CompositePart *cpart = dataHolder->getCompositePart(cpartId);

	if(cpart == NULL) {
		throw Sb7Exception();
	}

	dataHolder->deleteCompositePart(cpart);

	return 0;
}

/////////////////////////////
// StructuralModification3 //
/////////////////////////////

int sb7::LMStructuralModification3::run(int tid) const {
		// Anju: Commenting the write lock of stmbench so that Dominator based locking can be used
/*	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock()); */

	// generate random composite part id
	int cpartId = get_random()->nextInt(parameters.getMaxCompParts()) + 1;
	CompositePart *cpart = dataHolder->getCompositePart(cpartId);

	if(cpart == NULL) {
		throw Sb7Exception();
	}

	// generate random base assembly id
	int bassmId = get_random()->nextInt(parameters.getMaxBaseAssemblies()) + 1;
	BaseAssembly *bassm = dataHolder->getBaseAssembly(bassmId);

	if(bassm == NULL) {
		throw Sb7Exception();
	}
	// Anju -- start --
	int min = 0, max = 0;
	if(bassm->m_pre_number <= cpart->m_pre_number)
			min = bassm->m_pre_number;
	else
			min = cpart->m_pre_number;
	if(bassm->m_post_number >= cpart->m_post_number)
			max = bassm->m_post_number;
	else
			max = cpart->m_post_number;
	if(min == 0 || max == 0)
	{
			return 0;
	}
	int rlm_min, rlm_max;
	pthread_rwlock_t *lock = getDominatorLock(&min,&max, &rlm_min, &rlm_max);
	interval *inv = new interval(min,max, rlm_min, rlm_max, 1);
ab: if(!ICheck.IsOverlap(inv, 1, tid, dataHolder))	
		{
				// No overlap.. lock can be aquired.
				pthread_rwlock_wrlock(lock);
				bassm->addComponent(cpart); // Edge addition
				// Update the intervals of bassm if needed
				bool didIntervalChange = false;
				if(cpart->m_pre_number < bassm->m_pre_number)
				{
						bassm->m_pre_number = cpart->m_pre_number;
						didIntervalChange = true;
				}
				if(bassm->m_post_number < cpart->m_post_number)
				{
						bassm->m_post_number = cpart->m_post_number;
						didIntervalChange = true;
				}
				if(didIntervalChange)
				{
						// This implies a new descendant, which was not part of bassm's sub-hierarchy previously is added now.
/*						if(cpart->rlm_pre_number < bassm->rlm_pre_number)
								bassm->rlm_pre_number = cpart->rlm_pre_number;
						if(bassm->rlm_post_number < cpart->rlm_post_number)
								bassm->rlm_post_number = cpart->rlm_post_number; */
				// Now, do the interval updates to ancestors of bassm
				ComplexAssembly *cassm = bassm->getSuperAssembly();
				while(didIntervalChange) // Remember to use break statement inside
				{
					didIntervalChange = false;
					if(cassm->m_pre_number > cpart->m_pre_number)
					{
							cassm->m_pre_number = cpart->m_pre_number;
							didIntervalChange = true;
					}
					if(cassm->m_post_number < cpart->m_post_number)
					{
							cassm->m_post_number = cpart->m_post_number;
							didIntervalChange =  true;
					}
/*					if(cassm->rlm_pre_number > cpart->rlm_pre_number)
					{
							cassm->rlm_pre_number = cpart->rlm_pre_number;
							didIntervalChange = true;
					}
					if(cassm->rlm_post_number < cpart->rlm_post_number)
					{
							cassm->rlm_post_number = cpart->rlm_post_number;
							didIntervalChange =  true;
					} */
					if((cassm->m_pre_number == min) && (cassm->m_post_number == max))
					{
							// Reached dominator. No need to go further up.
							break;
					}
					cassm = cassm->getSuperAssembly(); // Pointer to parent
				}
				}

				ICheck.Delete(tid);
				pthread_rwlock_unlock(lock);
		}
		else
		{
				sleep(get_random()->nextInt(10));
				goto ab;
		}
	// Anju -- end --
//	bassm->addComponent(cpart);

	return 0;
}

/////////////////////////////
// StructuralModification4 //
/////////////////////////////
// Anju - Edge removal
int sb7::LMStructuralModification4::run(int tid) const {
		// Anju: Commenting the write lock of stmbench so that Dominator based locking can be used
/*	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());*/

	// generate random base assembly id
	int bassmId = get_random()->nextInt(parameters.getMaxBaseAssemblies()) + 1;
	BaseAssembly *bassm = dataHolder->getBaseAssembly(bassmId);

	if(bassm == NULL) {
		throw Sb7Exception();
	}	

	// select one of composite parts used in the base assembly
	Bag<CompositePart *> *cpartBag = bassm->getComponents();
	int compNum = cpartBag->size();

	if(compNum == 0) {
		throw Sb7Exception();
	}

	int nextId = get_random()->nextInt(compNum);

	// find component with that ordinal number
	BagIterator<CompositePart *> iter = cpartBag->getIter();
	int i = 0;

	while(iter.has_next()) 
	{
			CompositePart *cpart = iter.next();
//			CompositePart *prev_cpart;
/*			if(nextId == i + 1)
					prev_cpart = cpart;*/
			if(nextId == i) {
					// Anju -- start --
					int min = 0, max = 0;
					if(bassm->m_pre_number <= cpart->m_pre_number)
							min = bassm->m_pre_number;
					else
							min = cpart->m_pre_number;
					if(bassm->m_post_number >= cpart->m_post_number)
							max = bassm->m_post_number;
					else
							max = cpart->m_post_number;
					if(min == 0 || max == 0)
					{
							return 0;
					}
					int rlm_min, rlm_max;
					pthread_rwlock_t *lock = getDominatorLock(&min,&max, &rlm_min, &rlm_max);
					interval *inv = new interval(min,max, rlm_min, rlm_max, 1);
abc: if(!ICheck.IsOverlap(inv, 1, tid, dataHolder))
		 {
				 // No overlap.. lock can be aquired.
				 pthread_rwlock_wrlock(lock);
				 bassm->removeComponent(cpart);
				 // Updating intervals
				 /*bool didIntervalChange = false;
				 if(nextId == 0) // The cpart to which the edge from bassm was deletedwas the first child of bassm
				 {
						 cpart = iter.next();
						 if(cpart->m_pre_number > bassm->m_pre_number)
						 {
								 bassm->m_pre_number = cpart->m_pre_number;
								 didIntervalChange = true;
						 }	
				 }
				 else if(nextId == cpartBag->size() )
				 {
						if(prev_cpart->m_post_number < bassm->m_post_number)
						{
							bassm->m_post_number = prev_cpart->m_post_number;
							didIntervalChange = true;
						}
				 }
				 if(didIntervalChange)
					{
						// Propagate the shrinking of intervals till the dominator
					} */
				 ICheck.Delete(tid);
				 pthread_rwlock_unlock(lock);
				 return 0;
		 }
					else
					{
							sleep(get_random()->nextInt(10));
							goto abc;
					}
			}

			i++;
	}

	throw Sb7Exception(
		"SM4: concurrent modification of BaseAssembly.components!");
}

/////////////////////////////
// StructuralModification5 //
/////////////////////////////

int sb7::LMStructuralModification5::run(int tid) const {
	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());

	// generate random base assembly id
	int bassmId = get_random()->nextInt(parameters.getMaxBaseAssemblies()) + 1;
	BaseAssembly *bassm = dataHolder->getBaseAssembly(bassmId);

	if(bassm == NULL) {
		throw Sb7Exception();
	}	

	// create sibling base assembly
	dataHolder->createBaseAssembly(bassm->getSuperAssembly());

	return 0;
}

/////////////////////////////
// StructuralModification6 //
/////////////////////////////

int sb7::LMStructuralModification6::run(int tid) const {
	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());

	// generate random base assembly id
	int bassmId = get_random()->nextInt(parameters.getMaxBaseAssemblies()) + 1;
	BaseAssembly *bassm = dataHolder->getBaseAssembly(bassmId);

	if(bassm == NULL) {
		throw Sb7Exception();
	}	

	// get parent and check that it has at least one more child
	ComplexAssembly *cassm = bassm->getSuperAssembly();
	Set<Assembly *> *subAssmSet = cassm->getSubAssemblies();

	// don't let the tree break
	if(subAssmSet->size() == 1) {
		throw Sb7Exception();
	}

	dataHolder->deleteBaseAssembly(bassm);

	return 0;
}

/////////////////////////////
// StructuralModification7 //
/////////////////////////////

int sb7::LMStructuralModification7::run(int tid) const {
	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());

	// generate random complex assembly id
	int cassmId = get_random()->nextInt(
		parameters.getMaxComplexAssemblies()) + 1;
	ComplexAssembly *cassm = dataHolder->getComplexAssembly(cassmId);

	if(cassm == NULL) {
		throw Sb7Exception();
	}

	// create sub assembly
	dataHolder->createSubAssembly(cassm, parameters.getNumAssmPerAssm());

	return 1;
}

/////////////////////////////
// StructuralModification8 //
/////////////////////////////

int sb7::LMStructuralModification8::run(int tid) const {
	WriteLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());

	// generate random complex assembly id
	int cassmId = get_random()->nextInt(
		parameters.getMaxComplexAssemblies()) + 1;
	ComplexAssembly *cassm = dataHolder->getComplexAssembly(cassmId);

	if(cassm == NULL) {
		throw Sb7Exception();
	}

	// get super assembly
	ComplexAssembly *superAssm = cassm->getSuperAssembly();

	// don't continue if we got root complex assembly
	if(superAssm == NULL) {
		throw Sb7Exception();
	}

	// check if this would break the tree structure
	Set<Assembly *> *assmSet = superAssm->getSubAssemblies();

	if(assmSet->size() == 1) {
		throw Sb7Exception();
	}

	// delete selected complex assembly
	dataHolder->deleteComplexAssembly(cassm);

	return 1;
}
