#include "traversal_ops_lm.h"

#include "../../struct/module.h"
#include "../../struct/connection.h"
#include "../../sb7_exception.h"
#include "../../parameters.h"
#include "lock_srv_lm.h"
#include "../../thread/thread.h"
#include <stdio.h>
#include<limits.h>
#include <stack>

//long int dfscounter = 0;


// Anju ----------- Start ----------
int leafCounter = 0;
int sb7::LMTraversalReverseDFS::run(int tid) const 
{
	int retval = traverse(dataHolder->getModule()->getDesignRoot());
	cout<< "\nRoot pre-post from RLM order traversal:  " << dataHolder->getModule()->getDesignRoot() -> rlm_pre_number<<" and "<<dataHolder->getModule()->getDesignRoot() -> rlm_post_number << endl;
	return 0;
}

int sb7::LMTraversalReverseDFS::traverse(ComplexAssembly *cassm) const {
		Set<Assembly *> *subAssm = cassm->getSubAssemblies();
		SetIterator<Assembly *> iter = subAssm->getIter();
		bool childrenAreBase = cassm->areChildrenBaseAssemblies();
		int min = INT_MAX, max = 0;	
		stack<Assembly *> myChildren;
		while(iter.has_next()) 
				myChildren.push(iter.next()); // Adding to stack

		Assembly *currentChild;
		while(!(myChildren.empty())) // stack not empty
		{
				currentChild = myChildren.top(); // Getting the top element
				if(childrenAreBase)
						traverse((BaseAssembly *)currentChild);
				else
						traverse((ComplexAssembly *)currentChild);

				if(currentChild->rlm_pre_number < min)
						min = currentChild->rlm_pre_number;
				if(currentChild->rlm_post_number > max)
						max = currentChild->rlm_post_number;

				myChildren.pop(); // Removing the top element
		}

		cassm->rlm_pre_number = min;
		cassm->rlm_post_number = max;
//		cout << "cassm id " << cassm->getId() << ": (" << min << ", " << max << ")" << "dom : (" << cassm->m_pre_number << ", " << cassm->m_post_number << ")" << endl;
		return 0;
}


int sb7::LMTraversalReverseDFS::traverse(BaseAssembly *bassm) const {

//		cout << "In baseAssm code----" << endl;
		stack<CompositePart *> myChildren;
		int min = INT_MAX, max = 0;	
		BagIterator<CompositePart *> iter = bassm->getComponents()->getIter();
	
  	while(iter.has_next()) 
				myChildren.push(iter.next());
		
		CompositePart *currentChild;
		while(!(myChildren.empty())) // stack not empty
		{
				currentChild = myChildren.top(); // Getting the top element
						traverse((CompositePart *)currentChild);

				if(currentChild->rlm_pre_number < min)
						min = currentChild->rlm_pre_number;
				if(currentChild->rlm_post_number > max)
						max = currentChild->rlm_post_number;

				myChildren.pop(); // Removing the top element
		}
		bassm->rlm_pre_number = min;
		bassm->rlm_post_number = max;
		
//		cout << "bassm id " << bassm->getId() << ": (" << min << ", " << max << ")"<< "dom : (" << bassm->m_pre_number << ", " << bassm->m_post_number << ")"  << endl;
	return 0;
}


int sb7::LMTraversalReverseDFS::traverse(CompositePart *cpart) const 
{
	if(cpart->rlm_pre_number == 0)
	{
			leafCounter++;
			cpart->rlm_pre_number = leafCounter;
			cpart->rlm_post_number = leafCounter;
//		cout << "cpart id " << cpart->getId() << ": (" << leafCounter << ", " << leafCounter << ")" << "dom : (" << cpart->m_pre_number << ", " << cpart->m_post_number << ")" << endl;
			// Give the same rlm interval to all the atomic parts below it.
	}

	AtomicPart *rootPart = cpart->getRootPart();
  Set<AtomicPart *> visitedPartSet;
  int retval =  traverse(rootPart, visitedPartSet);	

	SetIterator<AtomicPart *> iter  = visitedPartSet.getIter();
  while(iter.has_next()) {
    AtomicPart *ap = iter.next();
    ap->rlm_pre_number = cpart->rlm_pre_number;
    ap->rlm_post_number = cpart->rlm_post_number;
    //cout << ap->getId() << endl;
  }

return retval;
}

int sb7::LMTraversalReverseDFS::traverse(AtomicPart *apart,
				Set<AtomicPart *> &visitedPartSet) const 
{
		int ret;
		if(apart != NULL && !(visitedPartSet.contains(apart))) 
		{
				visitedPartSet.add(apart);
				// visit all connected parts
				Set<Connection *> *toConns = apart->getToConnections();
				SetIterator<Connection *> iter = toConns->getIter();
				while(iter.has_next()) 
				{
						Connection *conn = iter.next();
						ret += traverse(conn->getDestination(), visitedPartSet);
				}
		}
		return ret;
}


// Anju ----------- End ----------


//***************************************************************************************************

int sb7::LMTraversalDFS::run(int tid) const {
	
	
	
	int retval = traverse(dataHolder->getModule()->getDesignRoot());
	cout<< "\nRoot pre-post:  " << dataHolder->getModule()->getDesignRoot() -> m_pre_number<<" and "<<dataHolder->getModule()->getDesignRoot() -> m_post_number << endl;

	return retval;
}

int sb7::LMTraversalDFS::traverse(ComplexAssembly *cassm) const {
	int partsVisited = 0;
	Set<Assembly *> *subAssm = cassm->getSubAssemblies();
	SetIterator<Assembly *> iter = subAssm->getIter();
	bool childrenAreBase = cassm->areChildrenBaseAssemblies();

	int pre = INT_MAX, post = 0;	

	// think about transforming this into a nicer oo design 
	while(iter.has_next()) {
		Assembly *assm = iter.next();

		if(!childrenAreBase) {
			partsVisited += traverse((ComplexAssembly *)assm);

		} else {
			partsVisited += traverse((BaseAssembly *)assm);
		}

		if(assm->m_pre_number < pre)
			pre = assm->m_pre_number;
		if(assm->m_post_number > post)
			post = assm->m_post_number;

	}

	cassm->m_pre_number = pre;
	cassm->m_post_number = post;


	return partsVisited;
}

int sb7::LMTraversalDFS::traverse(BaseAssembly *bassm) const {
	int partsVisited = 0;

	BagIterator<CompositePart *> iter = bassm->getComponents()->getIter();

	while(iter.has_next()) {
		partsVisited += traverse(iter.next());
	}
	return partsVisited;
}

int sb7::LMTraversalDFS::traverse(CompositePart *cpart) const {
	
//	if(cpart -> m_pre_number == 0)
//	cpart -> m_pre_number = dfscounter++;
//	else dfscounter++;

		if(cpart -> m_pre_number != cpart->m_post_number)
		{
	cout << cpart->getId() << ": (" << cpart -> m_pre_number << ", " << cpart->m_post_number << ")" << endl; 
		}

	AtomicPart *rootPart = cpart->getRootPart();
	Set<AtomicPart *> visitedPartSet;



	int retval =  traverse(rootPart, visitedPartSet);

	//cout << "Composite part id : " << cpart->getId() << "-------" << endl;
	// Assign intervals to all connected atomic parts
	SetIterator<AtomicPart *> iter	= visitedPartSet.getIter();
	while(iter.has_next()) {
		AtomicPart *ap = iter.next();
		ap->m_pre_number = cpart->m_pre_number;
		ap->m_post_number = cpart->m_post_number;
		//cout << ap->getId() << endl;
	}
	//cout << "-------" << endl;
	return retval;
}

int sb7::LMTraversalDFS::traverse(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	int ret;

	if(apart == NULL) {
		ret = 0;
	} else if(visitedPartSet.contains(apart)) {
		ret = 0;

	} else {


		ret = performOperationOnAtomicPart(apart, visitedPartSet);
		visitedPartSet.add(apart);

		// visit all connected parts
		Set<Connection *> *toConns = apart->getToConnections();
		SetIterator<Connection *> iter = toConns->getIter();

		while(iter.has_next()) {
			Connection *conn = iter.next();
			ret += traverse(conn->getDestination(), visitedPartSet);
		}
	}
	return ret;
}

int sb7::LMTraversalDFS::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	apart->nullOperation();
	return 1;
}



//***************************************************************************************************
////////////////
// Traversal1 //
////////////////

int sb7::LMTraversal1::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());
	int x = traverse(dataHolder->getModule()->getDesignRoot());
	std::cout<<" it is a traversal: "<<x;	
	return x;
}

int sb7::LMTraversal1::traverse(ComplexAssembly *cassm) const {
	int partsVisited = 0;

	Set<Assembly *> *subAssm = cassm->getSubAssemblies();
	SetIterator<Assembly *> iter = subAssm->getIter();
	bool childrenAreBase = cassm->areChildrenBaseAssemblies();

	// think about transforming this into a nicer oo design 
	while(iter.has_next()) {
		Assembly *assm = iter.next();

		if(!childrenAreBase) {
			partsVisited += traverse((ComplexAssembly *)assm);
		} else {
			partsVisited += traverse((BaseAssembly *)assm);
		}
	}

	return partsVisited;
}

int sb7::LMTraversal1::traverse(BaseAssembly *bassm) const {
	int partsVisited = 0;
	BagIterator<CompositePart *> iter = bassm->getComponents()->getIter();

	while(iter.has_next()) {
		partsVisited += traverse(iter.next());
	}

	return partsVisited;
}

int sb7::LMTraversal1::traverse(CompositePart *cpart) const {
	AtomicPart *rootPart = cpart->getRootPart();
	Set<AtomicPart *> visitedPartSet;
	return traverse(rootPart, visitedPartSet);
}

int sb7::LMTraversal1::traverse(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	int ret;

	if(apart == NULL) {
		ret = 0;
	} else if(visitedPartSet.contains(apart)) {
		ret = 0;
	} else {
		ret = performOperationOnAtomicPart(apart, visitedPartSet);
		visitedPartSet.add(apart);

		// visit all connected parts
		Set<Connection *> *toConns = apart->getToConnections();
		SetIterator<Connection *> iter = toConns->getIter();

		while(iter.has_next()) {
			Connection *conn = iter.next();
			ret += traverse(conn->getDestination(), visitedPartSet);
		}
	}

	return ret;
}

int sb7::LMTraversal1::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	apart->nullOperation();
	return 1;
}

/////////////////
// Traversal2a //
/////////////////

int sb7::LMTraversal2a::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	WriteLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());
	
	return traverse(dataHolder->getModule()->getDesignRoot());
}

int sb7::LMTraversal2a::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	int ret;

	if(visitedPartSet.size() == 0) {
		apart->swapXY();
		ret = 1;
	} else {
		apart->nullOperation();
		ret = 1;
	}

	return ret;
}

/////////////////
// Traversal2b //
/////////////////

int sb7::LMTraversal2b::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	WriteLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());

	return traverse(dataHolder->getModule()->getDesignRoot());
}

int sb7::LMTraversal2b::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	apart->swapXY();
	return 1;
}

/////////////////
// Traversal2c //
/////////////////

int sb7::LMTraversal2c::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	WriteLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());

	return traverse(dataHolder->getModule()->getDesignRoot());
}

int sb7::LMTraversal2c::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	apart->swapXY();
	apart->swapXY();
	apart->swapXY();
	apart->swapXY();

	return 4;
}

/////////////////
// Traversal3a //
/////////////////

int sb7::LMTraversal3a::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	WriteLockHandle apartLockHandle(lm_lock_srv.getAtomicPartLock());

	return traverse(dataHolder->getModule()->getDesignRoot());
}

int sb7::LMTraversal3a::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	int ret;

	if(visitedPartSet.size() == 0) {
		updateBuildDate(apart);
		ret = 1;
	} else {
		apart->nullOperation();
		ret = 1;
	}
	
	return ret;
}

void sb7::LMTraversal3a::updateBuildDate(AtomicPart *apart) const {
	dataHolder->removeAtomicPartFromBuildDateIndex(apart);
	apart->updateBuildDate();
	dataHolder->addAtomicPartToBuildDateIndex(apart);
}

/////////////////
// Traversal3b //
/////////////////

int sb7::LMTraversal3b::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	updateBuildDate(apart);
	return 1;
}

/////////////////
// Traversal3c //
/////////////////

int sb7::LMTraversal3c::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &visitedPartSet) const {
	updateBuildDate(apart);
	updateBuildDate(apart);
	updateBuildDate(apart);
	updateBuildDate(apart);
	return 4;
}

////////////////
// Traversal4 //
////////////////

int sb7::LMTraversal4::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle docLockHandle(lm_lock_srv.getDocumentLock());

	return LMTraversal1::traverse(dataHolder->getModule()->getDesignRoot());
}

int sb7::LMTraversal4::traverse(CompositePart *cpart) const {
	return traverse(cpart->getDocumentation());
}

int sb7::LMTraversal4::traverse(Document *doc) const {
	return doc->searchText('I');
}

int sb7::LMTraversal4::traverse(AtomicPart *part,
		Set<AtomicPart *> &setOfVisitedParts) const {
	throw Sb7Exception("T4: traverse(AtomicPart, HashSet<AtomicPart>) called!");
}

int sb7::LMTraversal4::performOperationOnAtomicPart(AtomicPart *apart,
		Set<AtomicPart *> &setOfVisitedPartIds) const {
	throw Sb7Exception("T4: performOperationInAtomicPart(..) called!");
}

////////////////
// Traversal5 //
////////////////

int sb7::LMTraversal5::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	WriteLockHandle docLockHandle(lm_lock_srv.getDocumentLock());

	return LMTraversal1::traverse(
		dataHolder->getModule()->getDesignRoot());
}

int sb7::LMTraversal5::traverse(Document *doc) const {
	int ret;

	if(doc->textBeginsWith("I am")) {
		ret = doc->replaceText("I am", "This is");
	} else if(doc->textBeginsWith("This is")) {
		ret = doc->replaceText("This is", "I am");
	} else {
		throw Sb7Exception(
			("T5: illegal document text: " + doc->getText()).c_str());
	}

	if(!ret) {
		throw Sb7Exception("T5: concurrent modification!");
	}

	return ret;
}

////////////////
// Traversal6 //
////////////////

int sb7::LMTraversal6::traverse(CompositePart *cpart) const {
	cpart->getRootPart()->nullOperation();
	return 1;
}

//////////////////////////////////////////////
// Traversal 7 - actually a short traversal //
//////////////////////////////////////////////

int sb7::LMTraversal7::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle assemblyLockHandle(
		lm_lock_srv.getAssemblyLockArray(),
		lm_lock_srv.getAssemblyLockArraySize());

	return innerRun(tid);
}

int sb7::LMTraversal7::innerRun(int tid) const {
	int apartId = get_random()->nextInt(parameters.getMaxAtomicParts()) + 1;
	Map<int, AtomicPart *> *apartIdx = dataHolder->getAtomicPartIdIndex();

	Map<int, AtomicPart *>::Query query;
	query.key = apartId;
	apartIdx->get(query);

	if(!query.found) {
		throw Sb7Exception();
	}

	return traverse(query.val->getPartOf());
}

int sb7::LMTraversal7::traverse(CompositePart *cpart) const {
	Set<Assembly *> visitedAssemblies;
	int ret = 0;

	BagIterator<BaseAssembly *> iter = cpart->getUsedIn()->getIter();

	while(iter.has_next()) {
		ret += traverse(iter.next(), visitedAssemblies);
	}

	return ret;
}

int sb7::LMTraversal7::traverse(Assembly *assembly,
		Set<Assembly *> &visitedAssemblies) const {
	int ret;

	if(assembly == NULL) {
		ret = 0;
	} else if(visitedAssemblies.contains(assembly)) {
		ret = 0;
	} else {
		visitedAssemblies.add(assembly);
		performOperationOnAssembly(assembly);
    	ret = traverse(assembly->getSuperAssembly(), visitedAssemblies) + 1;
	}

	return ret;
}

void sb7::LMTraversal7::performOperationOnAssembly(Assembly *assembly) const {
	assembly->nullOperation();
}

//////////////////////////////////////////
// Traversal8 - actually a ro operation //
//////////////////////////////////////////

int sb7::LMTraversal8::run(int tid) const {
	ReadLockHandle smLockHandle(lm_lock_srv.getStructureModificationLock());
	ReadLockHandle manLockHandle(lm_lock_srv.getManualLock());

	return traverse(dataHolder->getModule()->getManual());
}

int sb7::LMTraversal8::traverse(Manual *manual) const {
	return manual->countOccurences('I');
}

//////////////////////////////////////////
// Traversal9 - actually a ro operation //
//////////////////////////////////////////

int sb7::LMTraversal9::traverse(Manual *manual) const {
	return manual->checkFirstLastCharTheSame();
}
