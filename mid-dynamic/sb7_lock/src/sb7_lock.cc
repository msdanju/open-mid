#include <iostream>

#include "parameters.h"
#include "benchmark.h"
#include "interval.h"
#include "struct/assembly.h"
#include<stdio.h>
using namespace sb7;

IntervalCheck ICheck;
// Anju adding some variables
int numBuckets;
int numIterations;
int numNodesLocked;
int totalLockRejections;
int correctedFScountByTraversal;
int correctedFScountByMID;
int totalLockAttempts;

/*
int totalNumOps;
int strictSubsumptionCount;
int totalLockRejections;
int falseSubsumptionCount;
int *fsCountArray;
int *correctedFScountArray; */

int main(int argc, char **argv) {
	// initialize parameters
	if(parameters.init(argc, argv, cout)) {
		parameters.print(cout);
		totalLockRejections = 0;
		correctedFScountByTraversal = 0;
		correctedFScountByMID = 0;
		totalLockAttempts = 0;
	/*	
	  totalNumOps = 0;
		strictSubsumptionCount = 0;
		totalLockRejections = 0;
		falseSubsumptionCount = 0;
		fsCountArray = new int[parameters.getThreadNum()];
		correctedFScountArray = new int[parameters.getThreadNum()];
		for(int i = 0; i < parameters.getThreadNum(); i++)
		{
				fsCountArray[i] = 0;
				correctedFScountArray[i] = 0;
		} */
		// create benchmark object and run it
		Benchmark benchmark;
		benchmark.init();
		benchmark.start();
		benchmark.report(cout);
		/*std::cout << "totalLockRejections = " << totalLockRejections << endl;
    std::cout << "correctedFScountByMID = " << correctedFScountByMID << endl;
    std::cout << "correctedFScountByTraversal = " << correctedFScountByTraversal << endl;
    std::cout << "totalLockAttempts = " << totalLockAttempts << endl;
    std::cout << "LRejPercentTotal = " << 100 * ((float)totalLockRejections / totalLockAttempts) << "%" << endl;
    std::cout << "LRejCorrectedByMID = " << 100 * (((float)correctedFScountByMID)  / (totalLockRejections + correctedFScountByMID + correctedFScountByTraversal)) << "%" << endl;
    std::cout << "LRejCorrectedByTraversal = " << 100 * (((float)correctedFScountByTraversal)  / (totalLockRejections + correctedFScountByMID + correctedFScountByTraversal)) << "%" << endl;
    std::cout << "TotalLRejReverted = " << 100 * (((float)correctedFScountByTraversal + correctedFScountByMID)  / (totalLockRejections + correctedFScountByMID + correctedFScountByTraversal)) << "%" << endl;
*/
		/*std::cout << "strictSubsumptionCount = " << strictSubsumptionCount << endl;
		std::cout << "falseSubsumptionCount= " << falseSubsumptionCount << endl;
		std:: cout << "--------------" << endl;*/
/*		std::cout << "totalNumOps = " << totalNumOps << endl;
				int totalFS = 0;
				int totalCorrectedFS = 0;
		for(int i = 0; i < parameters.getThreadNum(); i++)
		{
				totalFS += fsCountArray[i];
				totalCorrectedFS += correctedFScountArray[i];
		}
	//	std::cout << "Using array for aggregating.. total num of FS = " << totalFS << endl;
		std::cout << "totalLockRejections = " << totalLockRejections << endl;
		std::cout << "% of lock rejections out of total ops = " << 100 * ((float)totalLockRejections / totalNumOps) << "%" << endl;
		std::cout << "% of FS out of total rejections = " << 100 * ((float)totalFS / totalLockRejections) << "%" << endl;
		std::cout << "% of FS out of total strict subsumptions = " << 100 * ((float)totalFS / strictSubsumptionCount) << "%" << endl;
		std::cout << "% of FS out of total number of operations = " << 100 * ((float)totalFS / totalNumOps) << "%" << endl;

		std::cout << "% of FS-rejections corrected with additional numbering = " << 100 * ((float)totalCorrectedFS / totalFS) << "%" << endl;
		std::cout << "Final % of lock rejections out of total ops = " << 100 * ((float)(totalLockRejections - totalCorrectedFS) / totalNumOps) << "%" << endl;
		delete[] fsCountArray;*/
	}
}
