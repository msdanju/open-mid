#ifndef SB7_DESIGN_OBJ_H_
#define SB7_DESIGN_OBJ_H_

#include <string>
#include <pthread.h>
using namespace std;

namespace sb7 {
	class DesignObj {
		public:
int m_id;
			DesignObj(int id, string type, int buildDate)
				: m_id(id), m_type(type), m_buildDate(buildDate) {
//*******************************************************************************************
			m_pre_number = 0;
			m_post_number = 0;
			pthread_rwlock_init(&NodeLock, NULL);
//*********************************************************************************************
			rlm_pre_number = 0;
			rlm_post_number = 0;
			 }

			virtual ~DesignObj() { }

			int getId() const {
				return m_id;
			}

			int getBuildDate() const {
				return m_buildDate;
			}

			void updateBuildDate();

			void nullOperation() const { }

			//**************************************
			int m_pre_number;
			int m_post_number;
			pthread_rwlock_t NodeLock;
			int rlm_pre_number;
			int rlm_post_number;
		

		protected:
			
			string m_type;
			int m_buildDate;
			
		
	};
}

#endif /*SB7_DESIGN_OBJ_H_*/
