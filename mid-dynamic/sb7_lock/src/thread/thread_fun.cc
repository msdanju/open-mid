#include <pthread.h>
#include <cmath>
#include <iostream>
#include <set>

#include "thread.h"
#include "thread_fun.h"

#include "../data_holder.h"
#include "../sb7_exception.h"
#include<vector>
// TODO move it to some header
#define MIN(a, b) ((a) < (b)) ? (a) : (b)

void *sb7::init_data_holder(void *data) {
	init_thread_init();

	DataHolder *dataHolder = (DataHolder *)data;
	dataHolder->init();

	thread_clean();

	// just return something
	return NULL;
}

void *sb7::worker_thread(void *data) {
	thread_init();

	WorkerThreadData *wtdata = (WorkerThreadData *)data;
			//printf("\n---------- THREAD ID = %d ---------------\n", wtdata->threadID);

	while(!wtdata->stopped) {
		int opind = wtdata->getOperationRndInd();
		// operation index pushback in vector

		
		Operation *op = wtdata->operations->getOperations()[opind];
		
		Operation localop = *(wtdata->operations->getOperations()[opind]);
		op->threadID = wtdata->threadID;
			//cout<< "\nthread ID "<<wtdata->threadID << endl;
		try {

		wtdata->operationId.push_back(opind);
			// get start time
			long start_time = get_time_ms();
			op->run(wtdata->threadID);
			//localop.run();			
			long end_time = get_time_ms();

			wtdata->successful_ops[opind]++;
			int ttc = (int)(end_time - start_time);

			if(ttc <= wtdata->max_low_ttc) {
				wtdata->operations_ttc[opind][ttc]++;
			} else {
				double logHighTtc = (::log(ttc) - wtdata->max_low_ttc_log) /
					wtdata->high_ttc_log_base;
				int intLogHighTtc =
					MIN((int)logHighTtc, wtdata->high_ttc_entries - 1);
				wtdata->operations_high_ttc_log[opind][intLogHighTtc]++;
			}

		} catch (Sb7Exception) {
			wtdata->failed_ops[opind]++;
		}
	}

	thread_clean();

	// just return something
	return NULL;
}



int sb7::WorkerThreadData::getOperationRndInd() const {
	double oprnd = get_random()->nextDouble();
	const std::vector<double> &opRat = operations->getOperationCdf();
	int opind = 0;

	while(opRat[opind] < oprnd) {
		opind++;
	}

	return opind;
}
