

#ifndef SB7_INTERVAL_H_
#define SB7_INTERVAL_H_

#include <vector>
#include <stdlib.h>
#include "operations/operations.h"
#include "containers.h"
#include "./struct/assembly.h"
#include "./struct/connection.h"
#include<string>
#include <vector>

#include "./data_holder.h"
#include "./thread/thread.h"
#include <pthread.h>

using namespace std;
using namespace sb7;
#define SIZE 40
extern int correctedByDL;
extern int totalLockRejections;
extern int correctedFScountByTraversal;
extern int correctedFScountByMID;
extern int totalLockAttempts;
extern int numTraversals;
extern long totalNumEdgesTraversed;

class interval{

public: int pre, post, rlm_pre, rlm_post, mode;
	interval( int a, int b, int c, int d, int m){
	pre = a; post = b; rlm_pre = c, rlm_post = d, mode = m;
	}
};

//**************************************************************************************************************
class IntervalCheck{
public:
interval *Array[SIZE];
pthread_rwlock_t ArrayLock[SIZE];

IntervalCheck()
{
	for(int i = 0;i<SIZE; i++)
	{
		
		Array[i] = NULL;
	int ret =	pthread_rwlock_init(&(ArrayLock[i]), NULL); // Anju added the lock init
	if(ret != 0)
	{
			printf("***There is some problem in initializing the RW lock on the pool \n***");
	}
//		pthread_rwlock_t ArrayLock[i]; // Anju commented this local def by Saurabh. 
	}
}

/* The below function returns true if there is a path from cassm to a node with interval (min,max). */
bool doesNodeReachInterval(ComplexAssembly *cassm, int min, int max, int* numLevelsTraversed )
{
		*numLevelsTraversed = (*numLevelsTraversed) + 1;
		if (*numLevelsTraversed > 3)
				return true; 
    Set<Assembly *> *subAssm = cassm->getSubAssemblies();
    SetIterator<Assembly *> iter = subAssm->getIter();
    bool childrenAreBase = cassm->areChildrenBaseAssemblies();
    while(iter.has_next())
    {
        Assembly *assm = iter.next();
        if((assm -> m_pre_number == min) && (assm -> m_post_number == max))
            return true; // The node with interval (min, max) is reachable from cassm.
        else if((assm -> m_pre_number <= min) && (assm -> m_post_number >= max))
        {
            bool isReachable;
            if(!childrenAreBase)
                isReachable = doesNodeReachInterval((ComplexAssembly *)assm, min, max, numLevelsTraversed);
            else
                isReachable = doesNodeReachInterval((BaseAssembly *)assm, min, max, numLevelsTraversed);
            if(isReachable)
                return true;
        }
    }
    return false; // The node with interval (min, max) can not be reached from cassm.
}

bool doesNodeReachInterval(BaseAssembly *bassm, int min, int max, int* numLevelsTraversed )
{
		*numLevelsTraversed = (*numLevelsTraversed) + 1;
		if (*numLevelsTraversed > 3)
				return true; 

    BagIterator<CompositePart *> iter = bassm->getComponents()->getIter();
    while(iter.has_next())
    {
        CompositePart *cpart = iter.next();
        if((cpart -> m_pre_number == min) && (cpart -> m_post_number == max))
            return true;
    }
    return false;
}

DesignObj* findNodeWithInterval(ComplexAssembly *cassm,  int min, int max, int *nodeType)
{
//    printf("Really Entered findNodeWithInterval for complex assem\n");
  if((cassm -> m_pre_number == min) && (cassm -> m_post_number == max) )
  {
      // This is the node with interval (min, max). Return it.
      *nodeType = 1; // 1 indicates complex assembly.
  //    printf("Found a complex assembly with interval (%d, %d). Returning.. \n", cassm -> m_pre_number, cassm -> m_post_number);
      return cassm;
  }
  else
  {
    // printf("Inside else part..\n");
    Set<Assembly *> *subAssm = cassm->getSubAssemblies();
    SetIterator<Assembly *> iter = subAssm->getIter();
    bool childrenAreBase = cassm->areChildrenBaseAssemblies();
    while(iter.has_next())
    {
      Assembly *assm = iter.next();
      if((assm -> m_pre_number <= min) && (assm -> m_post_number >= max) )
      {
          if(!childrenAreBase)
          {
              return findNodeWithInterval((ComplexAssembly *)assm, min, max, nodeType);
          }
          else
          {
              return findNodeWithInterval((BaseAssembly *)assm, min, max, nodeType);
          }
      }
    }
    *nodeType = 11; // Indicating invalid object
    return NULL;
  }

}

DesignObj* findNodeWithInterval(BaseAssembly *bassm,  int min, int max, int *nodeType)
{
//  printf("Entered findNodeWithInterval for base assem\n");
    if((bassm -> m_pre_number == min) && (bassm -> m_post_number == max) )
    {
        // This is the node with interval (min, max). Return it.
        *nodeType = 2; // 2 indicates base assembly.
  //    printf("Found a base assembly with interval (%d, %d). Returning.. \n", bassm -> m_pre_number, bassm -> m_post_number);
        return bassm;
    }
    else
    {
//        *nodeType = 10; // Indicating invalid object
//      return NULL;
        BagIterator<CompositePart *> iter = bassm->getComponents()->getIter();
        while(iter.has_next())
        {
            CompositePart *cpart = iter.next();
            if((cpart -> m_pre_number == min) && (cpart->m_post_number == max))
            {
              *nodeType = 3; // 3 indicates composite part.
              return cpart;
            }
        }
    }
    // Code should not be reaching here. If it reaches, there is something wrong with the numbering.
    *nodeType = 10; // Indicating invalid object
    return NULL;
}

bool traverseAndCheckIfFS(int larger_min, int larger_max, int smaller_min, int smaller_max, DataHolder *dataHolder)
    {
        int nodeType;
        nodeType = 0;
         DesignObj* myObj = findNodeWithInterval(dataHolder->getModule()->getDesignRoot(), larger_min, larger_max, &nodeType);
  //      printf("NodeType = %d\n", nodeType);
    //    printf("Using DesignObj.... will it work???\n");
      //  printf("Trying.. (%d, %d)\n", myObj->m_pre_number, myObj->m_post_number);

        // We got the pointer to the node with the larger interval. Now do a level-by-level traversal to see if it is falsely subsuming the smaller interval.
        bool isReachable = false;
				int numLevelsTraversed = 0 ;
        if(nodeType == 1)
        {
            isReachable = doesNodeReachInterval((ComplexAssembly *)myObj, smaller_min, smaller_max, &numLevelsTraversed);
        }
        else if(nodeType == 2)
        {
            isReachable = doesNodeReachInterval((BaseAssembly *)myObj, smaller_min, smaller_max, &numLevelsTraversed);
        }
			
				numTraversals++;
				totalNumEdgesTraversed += numLevelsTraversed;
				if(isReachable)
        {
            //printf("True subsumption identified... numLevelsTraversed = %d\n", numLevelsTraversed);
            return false; // smaller interval node is reachable. So not a false subsumption.
        }
        else
        {
            //printf("FS identified by traversal. numLevelsTraversed = %d\n", numLevelsTraversed);
            return true; // It is a false subsumption.
        }
    }


// Anju edited this function to handle the read requests' overlap check.
bool IsOverlap(interval *inv, int m, int threadID, DataHolder *dataHolder)
{
		pthread_rwlock_wrlock(&ArrayLock[0]); // Taking the lock on the pool.
		bool fsIdentifiedByDL = false;
		bool fsIdentifiedByMID =  false;
		bool fsIdentifiedByTraversal = false;
		totalLockAttempts++;
		for(int i=0; i< SIZE; i++)
		{
				if(Array[i] != NULL)
				{		
//						if((Array[i]->pre >= inv->pre && Array[i]->post <= inv->post) ||
	//							(Array[i]->pre <= inv->pre && Array[i]->post >= inv->post))
					if(Array[i]->pre <= inv->post && Array[i]->post >= inv->pre)
						{
								if(inv->mode == 0 && Array[i]->mode == 0)
								{
										continue; // The already locked interval and the current request are both in read mode. So this is NOT an actual overlap. We need to check if there is overlap with any other interval locked in write mode. So continuing the for loop.
								}
								else
								{
										// Check if the second numbering also says there is an overlap
										//if((Array[i]->rlm_pre >= inv->rlm_pre && Array[i]->rlm_post <= inv->rlm_post) || (Array[i]->rlm_pre <= inv->rlm_pre && Array[i]->rlm_post >= inv->rlm_post))
										if(Array[i]->rlm_pre <= inv->rlm_post && Array[i]->rlm_post >= inv->rlm_pre)
																	{
											 // Traverse and check if it is a false subsumption
                      bool isFS;
                      int larger_min, larger_max, smaller_min, smaller_max;
                      if((Array[i]->pre >= inv->pre && Array[i]->post <= inv->post))
                      {
                          larger_min = inv->pre; larger_max = inv->post; smaller_min = Array[i]->pre; smaller_max = Array[i]->post;
                      }
                      else
                      {
                          larger_min = Array[i]->pre; larger_max = Array[i]->post; smaller_min = inv->pre; smaller_max = inv->post;
                      }

                      isFS = traverseAndCheckIfFS(larger_min, larger_max, smaller_min, smaller_max, dataHolder);
                      if(isFS)
                      {
//                        correctedFScountByTraversal++;
												fsIdentifiedByTraversal = true;
                        continue;
                      }
                      else
                      {
                        // True overlap
                        totalLockRejections++;
                        pthread_rwlock_unlock(&ArrayLock[0]); // Unlocking the pool before returning.
                        return true;
                      }											
										}
										fsIdentifiedByMID =  true;
//									correctedFScountByMID++; // An instance of second numbering correcting the false subsumption
								}
						}
					fsIdentifiedByDL = true;
				}
		}

		Insert(inv, threadID);
		if(fsIdentifiedByMID)
				correctedFScountByMID++;
		else if(fsIdentifiedByDL)
				correctedByDL++;
		else if(fsIdentifiedByTraversal)
				correctedFScountByTraversal++;
		pthread_rwlock_unlock(&ArrayLock[0]); // Unlocking the pool before returning.
		return false;

}



void Insert(interval *inv, int index)
{

	//pthread_rwlock_wrlock(&ArrayLock[index]);	
	Array[index] = inv;
	//pthread_rwlock_unlock(&ArrayLock[index]);
	
}

void Delete(int index)
{//index=0;
	pthread_rwlock_wrlock(&ArrayLock[0]);
	
	Array[index] = NULL;
	pthread_rwlock_unlock(&ArrayLock[0]);
	

}

};

#endif
