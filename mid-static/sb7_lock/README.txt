To compile, use the Makefile provided.

Sample command to run the code:

./sb7_lock -s b -l m -n 20 -w rw -d 12000 -b 2000 -i 5000 -q 50

s: size of the graph
l: locking type (medium/coarse grained)
n: number of threads
w: workload
d: duration of parallel run in milli seconds
b: number of buckets
i: critical section size in number of iterations
q: number of nodes locked by a thread at a time


